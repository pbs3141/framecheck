#include <fstream>
#include <glib.h>

namespace {

std::ofstream fifo("/tmp/framecheck.txt");

struct Prof
{
    gint64 start;
    const char *name;
    int subtype;
    
    Prof() : start(-1) {}
    
    Prof(const char *name, int subtype = 0) : start(g_get_monotonic_time()), name(name), subtype(subtype) {}
    
    Prof(Prof &&p)
    {
        assign(p);
        p.start = -1;
    }
    
    Prof(const Prof &p)
    {
        assign(p);
    }
    
    ~Prof()
    {
        if (start != -1) write();
    }
    
    Prof &operator=(Prof &&p)
    {
        if (start != -1) write();
        assign(p);
        p.start = -1;
        return *this;
    }
    
    void assign(const Prof &p)
    {
        start = p.start;
        name = p.name;
        subtype = p.subtype;
    }
    
    void write()
    {
        fifo << name << ' ' << start << ' ' << g_get_monotonic_time() << ' ' << subtype << std::endl;
    }
};

}

#define FuncProf() Prof(__FUNCTION__)

#define framecheck_whole_function auto _framecheck_obj = FuncProf();
