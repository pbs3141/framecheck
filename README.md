# framecheck

## Description

Super-simple framerate analysis tool, originally written for debugging Inkscape, but can be used in any code.

Framecheck records timing events from your code and displays them in real time. There are three parts to framecheck:

* A log file, usually located at `/tmp/framecheck.txt`. This is where events are written to, one per line, in the format `name begin_us end_us subtype`.

* A convenience C++ header [framecheck.h](framecheck.h) for doing the above.

* A Qt frontend [src/](src) for displaying the events.

There's also a C header [framecheck_c.h](framecheck_c.h) if you need to debug a C program.

## Set-up

* Include the header into your project.

* Build and run the graphical frontend. (Easiest with [QtCreator](https://doc.qt.io/qtcreator/index.html).)

## Usage

* Add

      framecheck_whole_function

  to the start of your function to generate a profiling event for the whole duration of the function.

* Use

      auto prof = Prof(name, subtype = 0);

  to generate a profiling event which lasts until the object is destroyed.

## Frontend

Each event gets its own track, and different subtypes get different colours within the same track.

The vertical lines give an indication of the length of a frame (60 FPS by default).

![Screenshot of frontend](https://i.imgur.com/JcAdxEV.png)

Controls:

 * Horizontal scroll wheel --> Scroll through timeline.
 
 * Home / End --> Jump to beginning / end.
 
 * Left / right arrow keys --> Move left and right in the timeline.
 
 * Ctrl + Plus/Minus --> Zoom in/out by a factor of 2.
 
 * O --> Choose a different file to watch / analyse.
 
 * Ctrl + Shift + K --> Clear the current events from the screen.
 
 * Shift + Delete --> Delete the monitored file from the filesystem.
 
 * Q / Esc --> Exit.

If you are at the end, then you will follow along as more events arrive, just like in a terminal.

## Disclaimer

Only available in dark mode.
