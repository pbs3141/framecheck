#include "mainwindow.h"
#include <array>
#include <sstream>
#include <cmath>
#include <QPainter>
#include <QWheelEvent>
#include <QKeyEvent>
#include <QApplication>
#include <QStandardPaths>
#include <QScreen>
#include <QFileDialog>

constexpr int SEP = 2;
constexpr int MAR = 4;
constexpr int HGT = 18;
constexpr int FPS = 60;
constexpr double BACK = 0.27;

double modf(double x)
{
    double i;
    return std::modf(x, &i);
}

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    setWindowFlags(Qt::FramelessWindowHint| Qt::WindowSystemMenuHint);
    resize(QApplication::primaryScreen()->geometry().width(), 100);
    path = QDir::tempPath() + QString("/framecheck.txt");
    fileisempty = true;
    reset();
    tryopen();
    startTimer(100);
}

void MainWindow::reset()
{
    string.clear();
    events.clear();
    names.clear();
    begin = std::numeric_limits<qint64>::max();
    end = std::numeric_limits<qint64>::min();
    shift = 0;
    follow = true;
}

void MainWindow::tryopen()
{
    if (file.isOpen()) file.close();
    file.setFileName(path);
    if (file.open(QIODevice::ReadOnly)) process_data();
    update_height();
}

void MainWindow::deletefile()
{
    if (file.isOpen()) file.close();
    file.remove();
    reset();
}

void MainWindow::update_height()
{
    auto rect = geometry();
    rect.setHeight(names.empty() ? 100 : SEP + names.size() * (HGT + SEP));
    setGeometry(rect);
}

void MainWindow::process_data()
{
    if (!file.isOpen())
    {
        tryopen();
        if (!file.isOpen()) return;
    }

    std::array<char, 256> buf;
    while (true)
    {
        auto count = file.read(&buf[0], buf.size());
        if (count == 0) break;
        for (int i = 0; i < count; i++)
        {
            if (buf[i] == '\n')
            {
                process_line(std::move(string));
                string.clear();
            }
            else
            {
                string.push_back(buf[i]);
            }
        }
    }
}

void MainWindow::process_line(std::string line)
{
    std::stringstream ss(std::move(line));

    std::string name;
    Event event;

    ss >> name >> event.begin >> event.end >> event.subtype;
    event.type = get_type(std::move(name));

    begin = std::min(begin, event.begin);
    end = std::max(end, event.end);

    events.emplace_back(event);
    fileisempty = false;
}

int MainWindow::get_type(std::string name)
{
    for (int i = 0; i < names.size(); i++)
        if (names[i] == name)
            return i;

    names.emplace_back(std::move(name));
    update_height();
    return names.size() - 1;
}

double MainWindow::calc_tscale() const
{
    return (double)framewidth * FPS / 1'000'000;
}

void MainWindow::paintEvent(QPaintEvent*)
{
    int max_name_len = 0;
    for (const auto &name : names)
        max_name_len = std::max<int>(max_name_len, name.length());

    QFont font("Dejavu Sans Mono", 10);
    font.setStyleHint(QFont::Monospace);

    QPainter painter(this);
    painter.setFont(font);
    auto text_width = max_name_len * painter.fontMetrics().boundingRect('X').width();
    int eff_left = MAR + text_width + MAR;
    int eff_width = width() - eff_left;

    painter.setPen(Qt::NoPen);
    painter.setBrush(QColor::fromHslF(0, 0, BACK));
    painter.drawRect(0, 0, width(), height());

    if (names.empty()) {
        QString text = !file.isOpen()
                     ? "Waiting for file '" + path + "' to be created; press O to pick a different one."
                     : fileisempty
                     ? "File '" + path + "' currently empty; waiting for data to be written."
                     : "Output cleared; waiting for more data to be written.";
        painter.setPen(Qt::white);
        painter.drawText(geometry(), text, Qt::AlignCenter | Qt::AlignVCenter);
        return;
    }

    painter.save();
    painter.translate(0, SEP);
    for (int i = 0; i < names.size(); i++)
    {
        painter.setBrush(QColor::fromHslF(0, 0, 0.33));
        painter.setPen(Qt::NoPen);
        painter.drawRect(eff_left, 0, width() - eff_left, HGT);
        painter.setPen(Qt::white);
        painter.drawText(QRectF(MAR, 0, text_width, HGT), QString::fromStdString(names[i]), Qt::AlignLeft | Qt::AlignVCenter);
        painter.translate(0, HGT + SEP);
    }
    painter.restore();

    const double tscale = calc_tscale();

    if (!events.empty())
    {
        int max_shift = std::max((int)((end - begin) * tscale) - eff_width, 0);
        if (!follow) {shift = std::max(shift, 0); if (shift >= max_shift) follow = true;}
        if (follow) shift = max_shift;
    }

    painter.setPen(Qt::NoPen);
    for (auto it = events.rbegin(); it != events.rend(); ++it)
    {
        const auto &ev = *it;
        int x1 = (int)((ev.begin - begin) * tscale) - shift;
        int x2 = (int)((ev.end   - begin) * tscale) - shift;
        if (x1 > eff_width) continue;
        if (x2 < 0) break;
        x1 = std::max(x1, 0);
        x2 = std::min(x2, eff_width);
        painter.save();
        painter.translate(eff_left, SEP + ev.type * (HGT + SEP));
        painter.setBrush(QColor::fromHsvF((1 + 3 * modf(0.1 + 0.245898036 * ev.type + 0.618033989 * ev.subtype)) / 6, 0.5, 1));
        painter.drawRect(x1, 0, std::max(x2 - x1, 1), HGT);
        painter.restore();
    }

    for (int x = framewidth; x < eff_width; x += framewidth)
    {
        painter.save();
        painter.translate(MAR + text_width + MAR + x, SEP);

        QPen pen;
        pen.setColor(QColor::fromHslF(0, 0, BACK, 0.5));
        pen.setCapStyle(Qt::FlatCap);
        pen.setDashPattern({1, 1});
        painter.setPen(pen);

        painter.drawLine(0, 0, 0, names.size() * (HGT + SEP) - SEP);

        pen.setColor(QColor::fromHslF(0, 0, 1, 0.45));
        pen.setCapStyle(Qt::FlatCap);
        pen.setDashPattern({1, 1});
        pen.setDashOffset(1);
        painter.setPen(pen);

        painter.drawLine(0, 0, 0, names.size() * (HGT + SEP) - SEP);

        painter.restore();
    }
}

void MainWindow::timerEvent(QTimerEvent*)
{
    process_data();
    update();
}

void MainWindow::wheelEvent(QWheelEvent *ev)
{
    if (events.empty()) return;
    follow = false;
    shift -= ev->angleDelta().x();
    update();
}

void MainWindow::keyPressEvent(QKeyEvent *ev)
{
    switch (ev->key())
    {
    case Qt::Key_Left:
        shift -= geometry().width() / 2;
        update();
        break;
    case Qt::Key_Right:
        shift += geometry().width() / 2;
        update();
        break;
    case Qt::Key_Home:
        follow = false;
        shift = 0;
        update();
        break;
    case Qt::Key_End:
        follow = true;
        update();
        break;
   case Qt::Key_O:
    {
        QFileDialog dialog(this);
        dialog.setFileMode(QFileDialog::AnyFile);
        if (!dialog.exec()) break;
        auto files = dialog.selectedFiles();
        if (files.size() != 1) break;
        path = files.first();
        reset();
        tryopen();
        break;
    }
   case Qt::Key_K:
        if (ev->modifiers() & Qt::CTRL && ev->modifiers() & Qt::SHIFT) reset();
        break;
    case Qt::Key_Delete:
         if (ev->modifiers() & Qt::SHIFT) deletefile();
         break;
   case Qt::Key_Q:
   case Qt::Key_Escape:
        close();
        break;
    case Qt::Key_Plus:
    case Qt::Key_Equal:
        if (ev->modifiers() & Qt::CTRL)
        {
            double t0 = (width() / 2 + shift) / calc_tscale();
            framewidth *= 2;
            shift = t0 * calc_tscale() - width() / 2;
            update();
        }
        break;
    case Qt::Key_Minus:
    case Qt::Key_Underscore:
        if (ev->modifiers() & Qt::CTRL && framewidth >= 2)
        {
            double t0 = (width() / 2 + shift) / calc_tscale();
            framewidth /= 2;
            shift = t0 * calc_tscale() - width() / 2;
            update();
        }
        break;
    default:
        break;
    }
}
