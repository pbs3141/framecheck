#pragma once
#include <vector>
#include <string>
#include <limits>
#include <QMainWindow>
#include <QFile>

class MainWindow : public QMainWindow
{
    Q_OBJECT

    struct Event
    {
        int type, subtype;
        qint64 begin, end;
    };

    void reset();
    void tryopen();
    void deletefile();
    void update_height();
    void process_data();
    void process_line(std::string line);
    int get_type(std::string name);
    double calc_tscale() const;
    
    QString path;
    QFile file;

    std::string string;
    std::vector<Event> events;
    std::vector<std::string> names;

    qint64 begin;
    qint64 end;
    int shift;
    bool follow;
    int framewidth = 50;
    bool fileisempty;

    void paintEvent(QPaintEvent*) override;
    void timerEvent(QTimerEvent*) override;
    void wheelEvent(QWheelEvent*) override;
    void keyPressEvent(QKeyEvent*) override;

public:
    MainWindow(QWidget *parent = nullptr);
};
