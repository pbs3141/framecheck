QT += core gui widgets
CONFIG += c++14
QMAKE_CXXFLAGS += -Wno-sign-compare

SOURCES += \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    mainwindow.h
