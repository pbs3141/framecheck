#include <stdio.h>
#include <glib.h>

#define fcreset fct = g_get_monotonic_time();

#define fcbegin gint64 fcreset

#define fcend(name) { \
  gint64 t = g_get_monotonic_time(); \
  FILE *f = fopen("/tmp/framecheck.txt", "a"); \
  fprintf(f, #name " %li %li 0\n", fct, t); \
  fclose(f); \
  fct = t; \
}

#define fcend2(name) { \
  gint64 t = g_get_monotonic_time(); \
  FILE *f = fopen("/tmp/framecheck.txt", "a"); \
  fprintf(f, "%s %li %li 0\n", name, fct, t); \
  fclose(f); \
  fct = t; \
}
